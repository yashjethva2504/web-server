//tamplate engine use to load dynamic files - Handlebar

const path = require('path')
const express = require('express')
const hbs = require('hbs')
const geoCode = require('../src/utils/geoCode')
const forecast = require('../src/utils/forecast')
// console.log(__dirname)
// console.log(__filename)
// console.log(path.join(__dirname, '../public'))

const app = express()
const port = process.env.PORT || 3000

//Define paths for Express config
const public_dir_path = path.join(__dirname, '../public');
const viewPath = path.join(__dirname,'../templates/views')
const partialsPath = path.join(__dirname,'../templates/partials')

//Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewPath)
hbs.registerPartials(partialsPath)

//app.set('views','template') - Change the hbs views folder to template folder - Default folder for hbs views file is views folder 

//Setup static directory to serve
app.use(express.static(public_dir_path))

app.get(``, (req, res) => {
    res.render('index' , {
        title: "Weather",
        name : "Yash"
    })

    //render - Allows to render views
})

// app.get('/help', (req, res) => {
//     res.send([{
//         name : "Yash",
//         age : 27
//         },
//         {
//             name : "Jay",
//             age : 27
//         }
//     ])
// })

app.get('/help', (req, res) => {
    res.render('help',{
        title: "Help",
        name : "Yash"
    })
})

app.get('/weather', (req, res) => {

    if(!req.query.address)
    {
        return res.send({
            error: "You must provide a address"
        })
    }
    geoCode(req.query.address, (error, { latitude, longitude } = {}) => {
        if(error)
        {
            return res.send({error})
        }
        forecast(latitude, longitude, (forecastError, forecastData) => {
            if(forecastError)
            {
                return res.send({forecastError})
            }
            res.send({forecastData})
        })
    })
})

app.get('/products', (req, res) => {  

    if(!req.query.search)
    {
        return res.send({
            error: "You must provide a search term"
        })
    }
    res.send({
        products: []
    })
    
})

app.get('/help/*', (req, res) => {
    res.render('404',{
        title: '404',
        errorMessage : "Help artical Not Found",
        name: "Yash"
    })
})

app.get('*', (req, res) => {
    res.render('404',{
        title: '404',
        errorMessage : "Page Not Found",
        name: "Yash"
    })
})
//app.com
//app.com/help
//app.com/about

app.listen(port, () => {
    console.log('Server us up on port '+port)
})