const request = require('postman-request')



const forecast = (latitude,longitude, callback) => { 
    const url = "http://api.weatherstack.com/current?access_key=07b6f9f4c32fa824a83158113dc3416a&query="+latitude+","+longitude;

    request({ url, json : true},(error, { body }) => {
        console.log(body)
        if(error)
        {
            callback("Unable to connect")
        }
        else if(body.error) {
            callback("Unable to find location")
        }
        else{
            callback(undefined,body.location.name+", "+body.location.region +", "+body.location.country+" : It is currently "+body.current.temperature+ " degrees out. It feels like "+body.current.feelslike+" degrees out.")
        }
    })
}

module.exports = forecast